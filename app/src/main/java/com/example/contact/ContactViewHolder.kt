package com.example.contact

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val contactImage: ImageView = itemView.findViewById(R.id.contactImage)
    val contactName: TextView = itemView.findViewById(R.id.contactName)
    val contactNumber: TextView = itemView.findViewById(R.id.contactNumber)
}
