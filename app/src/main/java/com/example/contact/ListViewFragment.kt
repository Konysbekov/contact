package com.example.contact

import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment

class ListViewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list_view, container, false)
        val listView: ListView = view.findViewById(R.id.listView)
        val contacts = fetchContacts()
        if (contacts != null) {
            val fromColumns = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER)
            val toViews = intArrayOf(android.R.id.text1, android.R.id.text2)
            val adapter = SimpleCursorAdapter(requireContext(), android.R.layout.simple_list_item_2, contacts, fromColumns, toViews, 0)
            listView.adapter = adapter

            listView.setOnItemClickListener { _, _, position, _ ->
                contacts.moveToPosition(position)
                val phoneNumber = contacts.getString(contacts.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
                val dialIntent = Intent(Intent.ACTION_DIAL).apply {
                    data = Uri.parse("tel:$phoneNumber")
                }
                startActivity(dialIntent)
            }
        } else {
            Toast.makeText(requireContext(), "No contacts found", Toast.LENGTH_SHORT).show()
        }
        return view
    }

    private fun fetchContacts(): Cursor? {
        val contentResolver: ContentResolver = requireContext().contentResolver
        return contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null, null, null, null
        )
    }
}
