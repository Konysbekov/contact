package com.example.contact

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ContactListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var contactAdapter: ContactAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_contact_list, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        val contacts = fetchContacts()
        if (contacts.isNotEmpty()) {
            contactAdapter = ContactAdapter(requireContext(), contacts)
            recyclerView.adapter = contactAdapter
        } else {
            Toast.makeText(requireContext(), "No contacts found", Toast.LENGTH_SHORT).show()
        }
        return view
    }

    private fun fetchContacts(): List<Contact> {
        val contactList = mutableListOf<Contact>()
        val contentResolver: ContentResolver = requireContext().contentResolver
        val cursor: Cursor? = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null, null, null, null
        )

        cursor?.use {
            val nameIndex = it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            val phoneNumberIndex = it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val imageUriIndex = it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)

            while (it.moveToNext()) {
                val name = it.getStringOrNull(nameIndex) ?: ""
                val phoneNumber = it.getStringOrNull(phoneNumberIndex) ?: ""
                val imageUriString = it.getStringOrNull(imageUriIndex)
                val imageUri = imageUriString?.let { uri -> Uri.parse(uri) }

                contactList.add(Contact(name, phoneNumber, imageUri))
            }
        }
        return contactList
    }

    private fun Cursor.getStringOrNull(columnIndex: Int): String? {
        return if (columnIndex >= 0) getString(columnIndex) else null
    }
}
