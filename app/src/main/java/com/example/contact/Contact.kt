package com.example.contact

import android.net.Uri

data class Contact(
    val name: String,
    val phoneNumber: String,
    val imageUri: Uri?
)
